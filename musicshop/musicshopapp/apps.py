from django.apps import AppConfig


class MusicshopappConfig(AppConfig):
    name = 'musicshopapp'

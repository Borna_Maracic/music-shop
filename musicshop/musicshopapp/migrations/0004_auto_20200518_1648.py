# Generated by Django 3.0.5 on 2020-05-18 14:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('musicshopapp', '0003_auto_20200511_1648'),
    ]

    operations = [
        migrations.RenameField(
            model_name='artikl',
            old_name='Cijena',
            new_name='cijena',
        ),
        migrations.RenameField(
            model_name='artikl',
            old_name='Naziv',
            new_name='naziv',
        ),
        migrations.RenameField(
            model_name='artikl',
            old_name='Opis',
            new_name='opis',
        ),
        migrations.RenameField(
            model_name='artikl',
            old_name='Slika',
            new_name='slika',
        ),
        migrations.RenameField(
            model_name='clan',
            old_name='Email',
            new_name='email',
        ),
        migrations.RenameField(
            model_name='clan',
            old_name='Ime',
            new_name='ime',
        ),
        migrations.RenameField(
            model_name='clan',
            old_name='Prezime',
            new_name='prezime',
        ),
        migrations.RenameField(
            model_name='klasa',
            old_name='Naziv',
            new_name='naziv',
        ),
        migrations.RenameField(
            model_name='kosarica',
            old_name='UkupnaCijena',
            new_name='ukupnaCijena',
        ),
        migrations.RenameField(
            model_name='recenzija',
            old_name='Komentar',
            new_name='komentar',
        ),
        migrations.RenameField(
            model_name='uloga',
            old_name='Naziv',
            new_name='naziv',
        ),
    ]

# Generated by Django 3.0.5 on 2020-05-20 16:33

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('musicshopapp', '0004_auto_20200518_1648'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='recenzija',
            name='clan',
        ),
        migrations.AddField(
            model_name='recenzija',
            name='korisnik',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='Clan',
        ),
        migrations.DeleteModel(
            name='Uloga',
        ),
    ]

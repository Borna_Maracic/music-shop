# Generated by Django 3.0.5 on 2020-05-20 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('musicshopapp', '0005_auto_20200520_1833'),
    ]

    operations = [
        migrations.AddField(
            model_name='recenzija',
            name='ocjena',
            field=models.IntegerField(null=True),
        ),
    ]

# Generated by Django 3.0.5 on 2020-05-20 16:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('musicshopapp', '0006_recenzija_ocjena'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recenzija',
            old_name='korisnik',
            new_name='user',
        ),
    ]
